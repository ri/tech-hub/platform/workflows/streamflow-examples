cwlVersion: v1.2
class: Workflow

inputs:
  tarball: File
  name_of_file_to_extract: string

outputs:
  compiled_class:
    type: File
    outputSource: compile/classfile

steps:
  untar:
    run:
      class: CommandLineTool
      baseCommand:
      - tar
      - xvf
      inputs:
        tarfile:
          type: File
          inputBinding:
            position: 1
        name_of_file_to_extract:
          type: string
          inputBinding:
            position: 2
      outputs:
        extracted_file:
          type: File
          outputBinding:
            glob: "*.java"
    in:
      tarfile: tarball
      name_of_file_to_extract: name_of_file_to_extract
    out: [extracted_file]

  compile:
    run:
      class: CommandLineTool
      baseCommand: javac
      arguments:
      - -d
      - "$(runtime.outdir)"
      inputs:
        src:
          type: File
          inputBinding:
            position: 1
      outputs:
        classfile:
          type: File
          outputBinding:
            glob: "*.class"
    in:
      src: untar/extracted_file
    out: [classfile]
