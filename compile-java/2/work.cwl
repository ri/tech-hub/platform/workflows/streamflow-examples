cwlVersion: v1.2
class: Workflow

inputs:
  tarball: File
  names_of_files_to_extract: string[]

outputs:
  classfiles:
    type: File[]
    outputSource: compile/classfiles

steps:
  untar:
    run:
      class: CommandLineTool
      baseCommand:
      - tar
      - x
      - -zvo
      stdout: output.txt
      inputs:
        tarfile:
          type: File
          inputBinding:
            position: 1
            prefix: -f
        names_of_files_to_extract:
          type: string[]
          inputBinding:
            position: 2
      outputs:
        extracted_files:
          type: File[]
          outputBinding:
            glob: "*.java"
    in:
      tarfile: tarball
      names_of_files_to_extract: names_of_files_to_extract
    out: [extracted_files]

  compile:
    run:
      class: CommandLineTool
      baseCommand: javac
      arguments:
      - -d
      - "$(runtime.outdir)"
      inputs:
        src:
          type: File[]
          inputBinding:
            position: 1
      outputs:
        classfiles:
          type: File[]
          outputBinding:
            glob: "*.class"
    in:
      src: untar/extracted_files
    out: [classfiles]
