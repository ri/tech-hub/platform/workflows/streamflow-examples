# README

## 1. Prerequisites

### 1.1. Prepare a service account for running Streamflow

Create a serviceaccount for running Streamflow:

    kubectl create serviceaccount streamflow

Make our serviceaccount capable of installing Helm charts (see also: https://helm.sh/docs/topics/rbac/). For example, assuming that `streamflow` servica account lives inside the `default` namespace:

    kubectl create rolebinding streamflow-edit --clusterrole edit --serviceaccount default:streamflow

### 1.2. Prepare volume for job data

Create a PVC named `job-data` and make sure it can bind to a PV (lazily or eagerly). This PVC will be used to store input/output data for workflows.

For workflows that expect input as files, prepare those files under `JOB_ID/input` subpath of the volume. All output files will be under `JOB_ID/output` subpath.

## 2. Run a job

Prepare the configuration files under a kustomization directory `echo/1`. Then, apply:

    kubectl apply -k echo/1

When finished, examine logs, then delete job resources:

    kubectl delete -k echo/1
