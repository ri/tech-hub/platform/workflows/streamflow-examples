# An example of using STDIN to feed a command
cwlVersion: v1.2
class: Workflow

inputs:
  message1: string

outputs:
  out:
    type: File
    outputSource: uppercase/uppercase_message

steps:
  echo:
    run:
      class: CommandLineTool
      baseCommand:
      - echo
      - -n
      stdout: message.txt
      inputs:
        message:
          type: string
          inputBinding: {}
      outputs:
        out1:
          type: File
          outputBinding:
            glob: message.txt
    in:
      message: message1
    out: [out1]
  uppercase:
    run:
      class: CommandLineTool
      baseCommand:
      - tr
      - "[:lower:]"
      - "[:upper:]"
      stdout: output.txt
      # NOTE: pipe this file into stdin ...
      # https://github.com/common-workflow-language/cwltool/issues/1004
      stdin: $(inputs.message.path)
      inputs:
        message:
          type: File
          # Do not bind as input! (because it will pass an additional parameter to our command)
          #inputBinding: {}
      outputs:
        uppercase_message:
          type: File
          outputBinding:
            glob: output.txt
    in:
      message: echo/out1
    out: [uppercase_message]
