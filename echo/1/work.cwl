cwlVersion: v1.2
class: Workflow

requirements:
  InlineJavascriptRequirement: {}

inputs:
  message1: string

outputs:
  out:
    type: string
    outputSource: uppercase/uppercase_message

steps:
  echo:
    run:
      class: CommandLineTool
      baseCommand:
      - echo
      - -n
      stdout: output.txt
      inputs:
        message:
          type: string
          inputBinding: {}
      outputs:
        out1:
          type: string
          outputBinding:
            glob: output.txt
            loadContents: true
            outputEval: $(self[0].contents)
    in:
      message: message1
    out: [out1]
  uppercase:
    run:
      class: ExpressionTool
      requirements:
        InlineJavascriptRequirement: {}
      inputs:
        message: string
      outputs:
        uppercase_message: string
      expression: |
        ${ return {"uppercase_message": inputs.message.toUpperCase()}; }
    in:
      message: echo/out1
    out: [uppercase_message]
