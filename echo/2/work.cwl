cwlVersion: v1.2
class: Workflow

inputs:
  message1: string

outputs:
  out:
    type: File
    outputSource: uppercase/uppercase_message

steps:
  echo:
    run:
      class: CommandLineTool
      baseCommand:
      - echo
      - -n
      stdout: output.txt
      inputs:
        message:
          type: string
          inputBinding: {}
      outputs:
        out1:
          type: File
          outputBinding:
            glob: output.txt
    in:
      message: message1
    out: [out1]
  uppercase:
    run:
      class: CommandLineTool
      baseCommand:
      - awk
      - >-
        {print toupper($0);}
      stdout: output.txt
      inputs:
        message:
          type: File
          inputBinding: {}
      outputs:
        uppercase_message:
          type: File
          outputBinding:
            glob: output.txt
    in:
      message: echo/out1
    out: [uppercase_message]
